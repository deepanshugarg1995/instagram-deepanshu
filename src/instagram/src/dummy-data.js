const dummyData = [
  {
    id: "a",
    username: "philzcoffee",
    thumbnailUrl:
      "https://scontent-sin6-2.cdninstagram.com/vp/11fe8fac7a7a25d9054bc8a941bec838/5D35404C/t51.2885-19/s150x150/54248019_669515680135973_8003102232411111424_n.jpg?_nc_ht=scontent-sin6-2.cdninstagram.com",
    imageUrl:
      "https://scontent-sin6-2.cdninstagram.com/vp/b27c034b8f019c524af083dff5d23690/5D487FAB/t51.2885-15/e15/46558602_261196677854889_9220834027975796566_n.jpg?_nc_ht=scontent-sin6-2.cdninstagram.com",
    likes: 400,
    timestamp: "July 17th 2017, 12:42:40 pm",
    comments: [
      {
        id: "1",
        username: "philzcoffee",
        text:
          "We've got more than just delicious coffees to offer at our shops!"
      },
      {
        id: "2",
        username: "biancasaurus",
        text: "Looks delicious!"
      },
      {
        id: "3",
        username: "martinseludo",
        text: "Can't wait to try it!"
      }
    ]
  },
  {
    id: "b",
    username: "twitch",
    thumbnailUrl:
      "https://scontent-sin6-2.cdninstagram.com/vp/da97ea3c9300765eab6eead8950f310b/5D3FE035/t51.2885-19/s150x150/15623824_313297082405026_85367813652348928_n.jpg?_nc_ht=scontent-sin6-2.cdninstagram.com",
    imageUrl:
      "https://scontent-sin6-2.cdninstagram.com/vp/e2ff696626de21e4e79f732276eff774/5D3934D8/t51.2885-15/fr/e15/s1080x1080/47691312_1960075887361549_6389292444186975701_n.jpg?_nc_ht=scontent-sin6-2.cdninstagram.com",
    likes: 4307,
    timestamp: "July 15th 2017, 03:12:09 pm",
    comments: [
      {
        id: "4",
        username: "twitch",
        text: "Epic Street Fighter action here in Las Vegas at #EVO2017!"
      },
      {
        id: "5",
        username: "michaelmarzetta",
        text: "Omg that match was crazy"
      },
      {
        id: "6",
        username: "themexican_leprechaun",
        text: "What a setup"
      },
      {
        id: "7",
        username: "dennis_futbol",
        text: "It that injustice"
      },
      {
        id: "8",
        username: "dennis_futbol",
        text: "Is"
      }
    ]
  },
  {
    id: "c",
    username: "playhearthstone",
    // thumbnailUrl: 'https://instagram.fbna1-1.fna.fbcdn.net/vp/51d5b37438ae3a47df37b7ed3fda141f/5B4ABAA7/t51.2885-19/s150x150/13398400_140638743023210_310840336_a.jpg',
    thumbnailUrl:
      "https://scontent-sin6-2.cdninstagram.com/vp/97dcd04b0aadd001069bd5d27c375db4/5D2AD622/t51.2885-19/s150x150/17662424_614839085381720_1954752174617526272_a.jpg?_nc_ht=scontent-sin6-2.cdninstagram.com",
    // imageUrl: 'https://instagram.fbna1-1.fna.fbcdn.net/vp/22618be7ee2a2b676d13e8d70d7d5e08/5B40BF6B/t51.2885-15/e35/25038917_1978298569058775_6081161469041311744_n.jpg',
    imageUrl:
      "https://scontent-sin6-2.cdninstagram.com/vp/32936be73bc96f387021beb54e1e07dc/5D4631E2/t51.2885-15/fr/e15/s1080x1080/50564108_385786898865697_4214214290737514055_n.jpg?_nc_ht=scontent-sin6-2.cdninstagram.com",
    likes: 5306,
    timestamp: "July 14th 2017, 10:04:08 am",
    comments: [
      {
        id: "9",
        username: "playhearthstone",
        text:
          "Power alone is not to be feared. Fear instead those who wield it! #FrozenThrone #Expansion #DeathKnights"
      },
      {
        id: "10",
        username: "tapmelon",
        text:
          "Wish that death knight could be added as a new playable class in this expansion."
      },
      {
        id: "11",
        username: "micpetboudreau",
        text: "Can't wait"
      },
      {
        id: "12",
        username: "awaywetravel",
        text: "I <3 Hearthstone."
      },
      {
        id: "13",
        username: "awesomebt28",
        text: "I like how gul'dan looks so old and useless"
      }
    ]
  }
];

export default dummyData;
