import React from "react";
import ReactDOM from "react-dom";
function PostTitle(props) {
  return (
    <div className="posttitle">
      <img src={props.data.thumbnailUrl} className="post-icon" />
      <span className="id-name"> {props.data.username}</span>
    </div>
  );
}
export default PostTitle;
