import React from "react";
import ReactDOM from "react-dom";
import PostTitle from "./PostTitle";
import "./PostContainer.css";
import PostImage from "./PostImage";
import dummyData from "../../instagram/src/dummy-data";
import CommentSection from "../CommentSection/CommentSection";
function PostContainer(props) {
  console.log(dummyData);

  return (
    <div className="postcontainer">
      {dummyData.map((value, index) => {
        return (
          <div className="card">
            <PostTitle data={value} />
            <PostImage data={value} />
            <CommentSection data={value} />
          </div>
        );
      })}
    </div>
  );
}

export default PostContainer;
