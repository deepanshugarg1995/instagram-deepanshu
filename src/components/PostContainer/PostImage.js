// import React from "react";
import React from "react";
import ReactDOM from "react-dom";
// import "./PostContainer.css";
function PostImage(props) {
  return (
    <div className="postimage_div">
      <img src={props.data.imageUrl} className="postimage" />
    </div>
  );
}
// src={require("./philz.PNG")}
export default PostImage;
