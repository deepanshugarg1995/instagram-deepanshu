import React from "react";
import ReactDOM from "react-dom";
import SearchLogo from "./SearchLogo";
import SearchBox from "./SearchBox";
import "./SearchBar.css";
function SearchBar() {
  return (
    <div className="searchbar">
      <div className="searchbar-right">
        <img src={require("./msg.png")} className="searchbar-img" />
        <div className="seprate" />
        Instagram
      </div>
      <SearchBox class="searchinline" />
      <SearchLogo class="searchinline" />
    </div>
  );
}
export default SearchBar;
