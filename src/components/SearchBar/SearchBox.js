import React from "react";
import ReactDOM from "react-dom";
function SearchBox(props) {
  return (
    <div className={` searchbox ${props.class}`}>
      <input type="text" placeholder="Search" />
    </div>
  );
}

export default SearchBox;
