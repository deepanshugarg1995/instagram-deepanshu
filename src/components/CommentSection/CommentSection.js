import React from "react";
import ReactDOM from "react-dom";
import "./CommentSection.css";
function CommentSection(props) {
  return (
    <div className="comment-section">
      <div className="like-icon">
        <img src={require("./heart.jpg")} className="comment-icons" />
        <img src={require("./message.jpg")} className="comment-icons" />
      </div>
      <div>
        <span className="comment-likes">
          {props.data.likes}
          Likes
        </span>
      </div>
      <div className="showing-comment">
        {props.data.comments.map((value, index) => {
          return (
            <div key={value.id} className="comment">
              <span className="comment-username">{value.username}</span>
              <span>{value.text}</span>{" "}
            </div>
          );
        })}
      </div>

      <div>{props.data.timestamp}</div>
      <div className="adding-comment">
        <input placeholder="Add a Comment...." />
      </div>
    </div>
  );
}
export default CommentSection;
