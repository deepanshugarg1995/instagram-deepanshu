import React from "react";
import "./Footer.css";
function Footer() {
  return (
    <div className="footer-div">
      <img src={require("./comment.png")} className="footer-img" />
      <img src={require("./heart.jpg")} className="footer-img" />
      <img src={require("./message.jpg")} className="footer-img" />
      <img src={require("./reload.png")} className="footer-img" />
    </div>
  );
}
export default Footer;
